FROM node AS buildstage

WORKDIR /home/app

COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm run build

FROM nginx:1.23.1-alpine

COPY --from=buildstage /home/app/dist /usr/share/nginx/html

